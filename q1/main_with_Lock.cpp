#include "omp.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include  <sys/time.h>
#include "pthread.h"
#include "semaphore.h"

#define SIZE 1048576
#define RUN_NUM 5
#define NUM_THREADS 6

float arr[SIZE];

typedef struct {
    int beginArray;
    int endArray;
} partialMaxInput;

typedef struct {
    float localMax;
    int localIndex;
} partialMaxOutput;

pthread_mutex_t lock;

int parallelIndex;
float parallelMaxArr;

void* getPartialMax(void* arg) {
    partialMaxInput* inputArgs = (partialMaxInput*)arg;
    partialMaxOutput* localResult = (partialMaxOutput*) malloc(sizeof(partialMaxOutput));
    localResult->localIndex = -1;
    localResult->localMax = -1;
    for (int i = inputArgs->beginArray; i < inputArgs->endArray; i++) {
        if (localResult->localMax < arr[i]) {
            localResult->localMax = arr[i];
            localResult->localIndex = i;
        }
    }

    pthread_mutex_lock(&lock);
    if (parallelMaxArr < localResult->localMax) {
        parallelMaxArr = localResult->localMax;
        parallelIndex = localResult->localIndex;
    }
    pthread_mutex_unlock(&lock);

    pthread_exit(NULL);
}


int main() 
{
    printf("Student1 No. : 810196684, Student2 No. : 810196683\n");

    srand(time(0));
    double start, end;
    float speedUp;
	float time1, time2;

    float avgParallelTime = 0, avgSerialTime = 0, avgSpeedup = 0;
    for (int j = 0; j < RUN_NUM; j++) {
	    // Initialize vectors with random numbers
        for (long i = 0; i < SIZE; i++)
            arr[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));

        //serial
        
        int serialIndex = -1;
        float serialMaxArr = -1;
        start = omp_get_wtime();
        for (int i = 0; i < SIZE; i++)
        {
            if (arr[i] > serialMaxArr)
            {
                serialMaxArr = arr[i];
                serialIndex = i;
            }
        }
        end = omp_get_wtime();
        time1 = end - start;


        parallelIndex = -1;
        parallelMaxArr = -1;

        start = omp_get_wtime();
        pthread_mutex_init(&lock, NULL);
        pthread_t threadHandler[NUM_THREADS];
        partialMaxInput inputArgs[NUM_THREADS];

        for (int i = 0; i < NUM_THREADS; i++) {
            inputArgs[i] = {i*SIZE/NUM_THREADS, (i+1)*SIZE/NUM_THREADS};
        }
        
        for (int i = 0; i < NUM_THREADS; i++) {
            pthread_create(&threadHandler[i], NULL, getPartialMax, (void*) &inputArgs[i]);
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            pthread_join(threadHandler[i], NULL);
        }

        pthread_mutex_destroy(&lock);


        end = omp_get_wtime();
        time2 = end - start;

        printf("Run Num is %d\n", j);
        printf("Serial result arr[%d] = %f\n", serialIndex, serialMaxArr);
        printf("Parallel result arr[%d] = %f\n", parallelIndex, parallelMaxArr);

        printf("Serial run time: %f seconds\n", time1);
        printf("Parallel run time: %f seconds\n", time2);
        speedUp = (float) (time1)/(float) time2;
        printf("Speedup = %f\n\n", speedUp);

        avgParallelTime += time2;
        avgSerialTime += time1;
        avgSpeedup += speedUp;
    }

    printf("Final Result:\n");
    printf("Serial run time (avg): %f seconds\n", avgSerialTime/RUN_NUM);
    printf("Parallel run time (avg): %f seconds\n", avgParallelTime/RUN_NUM);
    printf("Speedup (avg) = %f\n\n", avgSpeedup/RUN_NUM);

}