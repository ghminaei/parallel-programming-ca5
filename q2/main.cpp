#include "shared.h"

using namespace std;

int main()
{
    printf("Student1 No. : 810196684, Student2 No. : 810196683\n");
    double serialTime, parallelTime;
    float *arr;
    arr = new float[N];
    for (long i = 0; i < N; i++)
        arr[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));

    float *serialArr;
    serialArr = new float[N];
    memcpy(serialArr, arr, N * sizeof(float));

    serialTime = serial(serialArr);
    printf("Serial time %f\n", serialTime);

    parallelTime = parallel(arr);
    printf("Parallel time %f\n", parallelTime);

    printf("Speedup is %f\n", serialTime/parallelTime);
    return 0;
}