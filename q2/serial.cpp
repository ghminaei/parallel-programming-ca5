#include "shared.h"

float serial(float arr[]) 
{ 
   	double start, stop;
   	start = omp_get_wtime();
	quickSort(arr, 0, N-1); 
	stop = omp_get_wtime();

	return stop - start; 
} 
