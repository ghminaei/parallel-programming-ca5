#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "omp.h"
#include "pthread.h"
#include "semaphore.h"
#include <iostream>

#define N 1048576
#define NUM_THREADS 8

float parallel(float arr[]);
float serial(float arr[]);
void swap(float *a, float *b);
int partition (float *arr, int low, int high);
void merge(float *arr, float *tmp, int lo, int hi);
void quickSort(float *arr, int low, int high);
