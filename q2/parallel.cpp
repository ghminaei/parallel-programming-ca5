#include "shared.h"

typedef struct {
    float* arr;
    int beginArray;
    int endArray;
    
} partialMaxInput;


void* quickSortP(void* arg) {
    partialMaxInput* inputArgs = (partialMaxInput*)arg;
    quickSort(inputArgs->arr, inputArgs->beginArray, inputArgs->endArray);

    pthread_exit(NULL);
}

float parallel(float arr[])
{
    double start, stop;
    float tmp[N];

    start = omp_get_wtime();

    pthread_t threadHandler[NUM_THREADS];
    partialMaxInput inputArgs[NUM_THREADS];

    for (int i = 0; i < NUM_THREADS; i++) {
        inputArgs[i] = {arr, i*N/NUM_THREADS, (i+1)*N/NUM_THREADS-1};
    }
    
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threadHandler[i], NULL, quickSortP, (void*) &inputArgs[i]);
    }

    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threadHandler[i], NULL);
    }

    merge(arr, tmp, 0, N/4);
    merge(arr, tmp, N/4, N/2);
    merge(arr, tmp, N/2, 3*N/4);
    merge(arr, tmp, 3*N/4, N);
    merge(arr, tmp, 0, N/2);
    merge(arr, tmp, N/2, N);
    merge(arr, tmp, 0, N);

    stop = omp_get_wtime();

    return stop - start;
}
