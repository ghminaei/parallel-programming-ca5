#include "shared.h"

void merge(float *arr, float *tmp, int lo, int hi)
{
    int i = lo;
    int mid = (hi - lo) / 2 + lo;
    int j = mid;
    int ti = lo;

    while (i < mid && j < hi)
    {
        if (arr[i] < arr[j])
        {
            tmp[ti] = arr[i];
            ti++;
            i++;
        } else
        {
            tmp[ti] = arr[j];
            ti++;
            j++;
        }
    }
    while (i < mid)
    {
        tmp[ti] = arr[i];
        ti++;
        i++;
    }
        while (j < hi)
        {
            tmp[ti] = arr[j];
            ti++;
            j++;
    }

    memcpy(&arr[lo], &tmp[lo], (hi - lo) * sizeof(float));
}


void swap(float *a, float *b) 
{ 
	float t = *a; 
	*a = *b; 
	*b = t; 
} 

int partition (float *arr, int low, int high) 
{ 
	float pivot = arr[high];
	int i = (low - 1);

	for (int j = low; j <= high - 1; j++) 
	{ 
		if (arr[j] <= pivot) 
		{ 
			i++;
			swap(&arr[i], &arr[j]); 
		} 
	} 
	swap(&arr[i + 1], &arr[high]); 
	return (i + 1); 
} 

void quickSort(float *arr, int low, int high) 
{ 
	if (low < high) 
	{ 
		int pi = partition(arr, low, high); 

		quickSort(arr, low, pi - 1); 
		quickSort(arr, pi + 1, high); 
	} 
} 